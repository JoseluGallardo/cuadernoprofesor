package com.jsw.acdat_cuaderno.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jsw.acdat_cuaderno.Modelo.Falta;
import com.jsw.acdat_cuaderno.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joselu on 2/3/17.
 */

public class FaltasAdapter extends RecyclerView.Adapter<FaltasAdapter.MyViewHolder> {
    private static FaltasAdapter mAdapter;
    private List<Falta> sitesList;
    private Context context;

    private FaltasAdapter(Context context) {
        this.sitesList = new ArrayList<Falta>();
        this.context = context;
    }

    public static FaltasAdapter getInstance(Context context) {
        if (mAdapter == null)
            mAdapter = new FaltasAdapter(context);

        return mAdapter;
    }

    @Override
    public FaltasAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.layout_falta, parent, false);
        return new FaltasAdapter.MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(FaltasAdapter.MyViewHolder holder, int position) {
        Falta feed = sitesList.get(position);
        holder.mNombre.setText(feed.getNombre());
        holder.mApellido.setText(feed.getApellido());
        holder.mFaltas.setText(feed.getFalta());
        holder.mTrabajo.setText(feed.getTrabajo());
        holder.mActitud.setText(feed.getActitud());
        holder.mObservaciones.setText(feed.getObservaciones());
    }

    @Override
    public int getItemCount() {
        return sitesList.size();
    }

    public void set(ArrayList<Falta> faltas) {
        if (sitesList != null) {
            this.sitesList = faltas;
            notifyDataSetChanged();
        }
    }

    public Falta getAt(int position) {
        //get the site in the position
        return this.sitesList.get(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView mNombre, mApellido, mFaltas, mTrabajo, mActitud, mObservaciones;

        public MyViewHolder(View itemView) {
            super(itemView);
            mNombre = (TextView) itemView.findViewById(R.id.tv_falta_nombre);
            mApellido = (TextView) itemView.findViewById(R.id.tv_falta_apellido);
            mFaltas = (TextView) itemView.findViewById(R.id.tv_falta);
            mTrabajo = (TextView) itemView.findViewById(R.id.tv_trabajo);
            mActitud = (TextView) itemView.findViewById(R.id.tv_actitud);
            mObservaciones = (TextView) itemView.findViewById(R.id.tv_observaciones);
        }
    }
}

package com.jsw.acdat_cuaderno.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jsw.acdat_cuaderno.Modelo.Alumno;
import com.jsw.acdat_cuaderno.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by joselu on 20/02/17.
 */

public class AlumnoAdapter extends RecyclerView.Adapter<AlumnoAdapter.MyViewHolder> {
    private static AlumnoAdapter mAdapter;
    private List<Alumno> sitesList;
    private Context context;

    private AlumnoAdapter(Context context) {
        this.sitesList = new ArrayList<Alumno>();
        this.context = context;
    }

    public static AlumnoAdapter getInstance(Context context) {
        if (mAdapter == null)
            mAdapter = new AlumnoAdapter(context);

        return mAdapter;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rootView = LayoutInflater.from(context).inflate(R.layout.layout_alumno, parent, false);
        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Alumno feed = sitesList.get(position);
        Picasso.with(context).load(feed.getImagen()).into(holder.mImage);
        holder.mNombre.setText(feed.getNombre());
        holder.mApellido.setText(feed.getApellido());
    }

    @Override
    public int getItemCount() {
        return sitesList.size();
    }

    public void set(ArrayList<Alumno> sitesList) {
        //set all the sites
        if (sitesList != null) {
            this.sitesList = (sitesList);
            notifyDataSetChanged();
        }
    }

    public Alumno getAt(int position) {
        //get the site in the position
        return this.sitesList.get(position);
    }

    public void add(Alumno site) {
        //add a site
        this.sitesList.add(site);
        //notifyDataSetChanged();
        notifyItemChanged(sitesList.size() - 1);
        notifyItemRangeChanged(0, sitesList.size() - 1, 1);
    }

    public void modifyAt(Alumno site, int position) {
        //modify a site in the position
        this.sitesList.set(position, site);
        notifyItemChanged(position);

    }

    public void removeAt(int position) {
        //delete a site in the position
        this.sitesList.remove(position);
        notifyItemRemoved(position);
    }

    public List<String> getMails() {

        List<String> mails = new ArrayList<>();

        for (Alumno tmp : this.sitesList) {
            mails.add(tmp.getEmail());
        }

        return mails;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView mImage;
        TextView mNombre, mApellido;

        public MyViewHolder(View itemView) {
            super(itemView);
            mImage = (CircleImageView) itemView.findViewById(R.id.iv_cardUser);
            mNombre = (TextView) itemView.findViewById(R.id.tv_cardName);
            mApellido = (TextView) itemView.findViewById(R.id.tv_cardApellido);
        }
    }
}

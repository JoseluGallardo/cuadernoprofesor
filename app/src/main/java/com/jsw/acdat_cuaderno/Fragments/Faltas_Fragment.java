package com.jsw.acdat_cuaderno.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jsw.acdat_cuaderno.Adapter.FaltasAdapter;
import com.jsw.acdat_cuaderno.Interfaces.ClickListener;
import com.jsw.acdat_cuaderno.R;
import com.jsw.acdat_cuaderno.Utils.CustomRecyclerListener;
import com.jsw.acdat_cuaderno.Utils.RestClient;
import com.jsw.acdat_cuaderno.Utils.Result;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class Faltas_Fragment extends Fragment {


    private FaltasAdapter mAdapter;
    private TextView mFecha;
    private RecyclerView mList;

    public Faltas_Fragment() {
        // Required empty public constructor
    }

    public static Faltas_Fragment getInstance() {
        return new Faltas_Fragment();
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = FaltasAdapter.getInstance(getContext());
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_faltas, container, false);
        mList = (RecyclerView) root.findViewById(R.id.rv_faltas);
        mFecha = (TextView) root.findViewById(R.id.tv_fecha);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mList.setHasFixedSize(true);

        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        mList.setLayoutManager(mLayoutManager);
        mList.setAdapter(mAdapter);
        LinearSnapHelper mCenterScroll = new LinearSnapHelper();
        mCenterScroll.attachToRecyclerView(mList);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        mFecha.setText(sdf.format(new Date()));
        mList.addOnItemTouchListener(new CustomRecyclerListener(getContext(), mList, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Bundle potatoe = new Bundle();
                potatoe.putSerializable("falta", mAdapter.getAt(position));
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, EditFalta_Fragment.getInstance(potatoe)).addToBackStack("Faltas").commit();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        descargaFaltas();
    }

    private void descargaFaltas() {
        final ProgressDialog progreso = new ProgressDialog(getContext());
        RestClient.get("/faltas", new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                //progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // Handle resulting parsed JSON response here
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        //put the sites in the adapter
                        mAdapter.set(result.getFaltas());
                        message = "Connection OK";
                    } else
                        message = result.getMessage();
                else
                    message = "Null data";
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getActivity(), throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}

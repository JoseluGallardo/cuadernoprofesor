package com.jsw.acdat_cuaderno.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jsw.acdat_cuaderno.Modelo.Falta;
import com.jsw.acdat_cuaderno.R;
import com.jsw.acdat_cuaderno.Utils.RestClient;
import com.jsw.acdat_cuaderno.Utils.Result;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditFalta_Fragment extends Fragment {

    @BindView(R.id.til_falta_nombre)
    TextInputLayout mNombre;
    @BindView(R.id.til_falta_apellidos)
    TextInputLayout mApellidos;
    @BindView(R.id.til_observaciones)
    TextInputLayout mObservaciones;
    @BindView(R.id.spn_falta)
    Spinner mFalta;
    @BindView(R.id.spn_actitud)
    Spinner mActitud;
    @BindView(R.id.spn_trabajo)
    Spinner mTrabajo;
    @BindView(R.id.fab_save_falta)
    FloatingActionButton mSave;
    private Falta tmp;

    public EditFalta_Fragment() {
        // Required empty public constructor
    }

    public static EditFalta_Fragment getInstance(Bundle arguments) {
        EditFalta_Fragment fragment = new EditFalta_Fragment();

        if (arguments != null)
            fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_edit_falta, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle potatoe = this.getArguments();

        if (potatoe != null) {
            tmp = (Falta) potatoe.getSerializable("falta");
            mApellidos.setEnabled(false);
            mNombre.setEnabled(false);
            mNombre.getEditText().setText(tmp.getNombre());
            mApellidos.getEditText().setText(tmp.getApellido());
            if (tmp.getObservaciones() != null)
                mObservaciones.getEditText().setText(tmp.getObservaciones());
        }

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveFalta();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    private void saveFalta() {
        final ProgressDialog progreso = new ProgressDialog(getContext());
        RequestParams params = new RequestParams();
        try {
            params.put("falta", mFalta.getSelectedItem().toString());
            params.put("trabajo", mTrabajo.getSelectedItem().toString());
            params.put("actitud", mActitud.getSelectedItem().toString());
            params.put("observaciones", mObservaciones.getEditText().getText());
            params.put("id", tmp.getId());
        } catch (Exception ex) {
            Toast.makeText(getContext(), "Hay parámetro vacíos...", Toast.LENGTH_LONG);
        }

        RestClient.put("/faltas/" + tmp.getId(), params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message = "";
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode())
                        message = "Added site ok";
                    else
                        message = "Error adding the site:\n" + result.getMessage();
                else
                    message = "Null data";

                //Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                //Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                //Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}

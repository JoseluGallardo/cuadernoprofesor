package com.jsw.acdat_cuaderno.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jsw.acdat_cuaderno.Modelo.Alumno;
import com.jsw.acdat_cuaderno.R;
import com.jsw.acdat_cuaderno.Utils.RestClient;
import com.jsw.acdat_cuaderno.Utils.Result;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class AddAlumno_Fragment extends Fragment {

    @BindView(R.id.til_nombre)
    TextInputLayout mNombre;
    @BindView(R.id.til_apellidos)
    TextInputLayout mApellidos;
    @BindView(R.id.til_direccion)
    TextInputLayout mDireccion;
    @BindView(R.id.til_ciudad)
    TextInputLayout mCiudad;
    @BindView(R.id.til_codPostal)
    TextInputLayout mCodPostal;
    @BindView(R.id.til_imageurl)
    TextInputLayout mImage;
    @BindView(R.id.til_telefono)
    TextInputLayout mTelefono;
    @BindView(R.id.til_email)
    TextInputLayout mEmail;
    @BindView(R.id.fab_save_alumn)
    FloatingActionButton mSave;
    private boolean editar = false;
    private String oldId;

    public AddAlumno_Fragment() {
        // Required empty public constructor
    }

    public static AddAlumno_Fragment getInstance(Bundle arguments) {
        AddAlumno_Fragment fragment = new AddAlumno_Fragment();

        if (arguments != null)
            fragment.setArguments(arguments);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add_alumno, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle potatoe = this.getArguments();

        if (potatoe != null) {
            editar = true;
            Alumno tmp = (Alumno) potatoe.getSerializable("alumno");
            mNombre.getEditText().setText(tmp.getNombre());
            mApellidos.getEditText().setText(tmp.getApellido());
            mDireccion.getEditText().setText(tmp.getDireccion());
            mCiudad.getEditText().setText(tmp.getCiudad());
            mCodPostal.getEditText().setText(tmp.getCodPostal());
            mImage.getEditText().setText(tmp.getImagen());
            mTelefono.getEditText().setText(tmp.getTelefono());
            mEmail.getEditText().setText(tmp.getEmail());
            oldId = tmp.getEmail();
        }

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editar)
                    editAlumno();
                else
                    addAlumno();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
    }

    private void addAlumno() {
        final ProgressDialog progreso = new ProgressDialog(getContext());
        RequestParams params = new RequestParams();
        try {
            params.put("imagen", mImage.getEditText().getText());
            params.put("nombre", mNombre.getEditText().getText());
            params.put("apellido", mApellidos.getEditText().getText());
            params.put("direccion", mDireccion.getEditText().getText());
            params.put("ciudad", mCiudad.getEditText().getText());
            params.put("codPostal", mCodPostal.getEditText().getText());
            params.put("telefono", mTelefono.getEditText().getText());
            params.put("email", mEmail.getEditText().getText());
        } catch (Exception ex) {
            Toast.makeText(getContext(), "Hay parámetro vacíos...", Toast.LENGTH_LONG);
        }

        RestClient.post("alumnos", params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message = "";
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode())
                        message = "Added site ok";
                    else
                        message = "Error adding the site:\n" + result.getMessage();
                else
                    message = "Null data";

                //Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void editAlumno() {
        final ProgressDialog progreso = new ProgressDialog(getContext());
        RequestParams params = new RequestParams();
        try {
            params.put("imagen", mImage.getEditText().getText());
            params.put("nombre", mNombre.getEditText().getText());
            params.put("apellido", mApellidos.getEditText().getText());
            params.put("direccion", mDireccion.getEditText().getText());
            params.put("ciudad", mCiudad.getEditText().getText());
            params.put("codPostal", mCodPostal.getEditText().getText());
            params.put("telefono", mTelefono.getEditText().getText());
            params.put("email", mEmail.getEditText().getText());
            params.put("id", oldId);
        } catch (Exception ex) {
            Toast.makeText(getContext(), "Hay parámetro vacíos...", Toast.LENGTH_LONG);
        }

        RestClient.put("alumnos/" + oldId, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message = "";
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode())
                        message = "Added site ok";
                    else
                        message = "Error adding the site:\n" + result.getMessage();
                else
                    message = "Null data";
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progreso.dismiss();
            }
        });
    }

}

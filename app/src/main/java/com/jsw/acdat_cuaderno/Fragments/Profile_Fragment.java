package com.jsw.acdat_cuaderno.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jsw.acdat_cuaderno.Modelo.Alumno;
import com.jsw.acdat_cuaderno.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class Profile_Fragment extends Fragment {

    private static Alumno mAlumno;
    private static Profile_Fragment mFragment;
    private CircleImageView mImage;
    private TextView mNombre, mApellido, mDireccion, mCiudad, mCodPostal, mTelefono, mEmail;

    public Profile_Fragment() {
        // Required empty public constructor
    }

    public static Profile_Fragment getInstance(Alumno alumno) {
        if (mFragment == null)
            mFragment = new Profile_Fragment();
        mAlumno = alumno;
        return mFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        mImage = (CircleImageView) root.findViewById(R.id.iv_profile_image);
        mNombre = (TextView) root.findViewById(R.id.tv_profile_name);
        mApellido = (TextView) root.findViewById(R.id.tv_profile_apellido);
        mDireccion = (TextView) root.findViewById(R.id.tv_direccion);
        mCiudad = (TextView) root.findViewById(R.id.tv_ciudad);
        mCodPostal = (TextView) root.findViewById(R.id.tv_codPostal);
        mTelefono = (TextView) root.findViewById(R.id.tv_telefono);
        mEmail = (TextView) root.findViewById(R.id.tv_email);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Picasso.with(getContext()).load(mAlumno.getImagen()).into(mImage);
        mNombre.setText(mAlumno.getNombre());
        mApellido.setText(mAlumno.getApellido());
        mDireccion.setText(mAlumno.getDireccion());
        mCiudad.setText(mAlumno.getCiudad());
        mCodPostal.setText(mAlumno.getCodPostal());
        mTelefono.setText(mAlumno.getTelefono());
        mEmail.setText(mAlumno.getEmail());
    }
}

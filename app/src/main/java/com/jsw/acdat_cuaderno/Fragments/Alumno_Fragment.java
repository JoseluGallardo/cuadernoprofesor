package com.jsw.acdat_cuaderno.Fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jsw.acdat_cuaderno.Adapter.AlumnoAdapter;
import com.jsw.acdat_cuaderno.Interfaces.ClickListener;
import com.jsw.acdat_cuaderno.Modelo.Alumno;
import com.jsw.acdat_cuaderno.R;
import com.jsw.acdat_cuaderno.Utils.CustomRecyclerListener;
import com.jsw.acdat_cuaderno.Utils.RestClient;
import com.jsw.acdat_cuaderno.Utils.Result;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class Alumno_Fragment extends Fragment {

    public static final String URL_API = "alumnos";
    public static final String MAIL = "mail";
    public static final int ADD_CODE = 100;
    public static final int UPDATE_CODE = 200;
    public static final int OK = 1;
    private static Alumno_Fragment mFragment;
    private static FloatingActionButton mFabAdd;
    private AlumnoAdapter mAdapter;
    private RecyclerView mRecycler;

    public Alumno_Fragment() {
        // Required empty public constructor
    }

    public static Alumno_Fragment getInstance() {
        if (mFragment == null)
            mFragment = new Alumno_Fragment();
        return mFragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = AlumnoAdapter.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_alumno, container, false);
        mRecycler = (RecyclerView) rootView.findViewById(R.id.rv_alumnos);
        mFabAdd = (FloatingActionButton) rootView.findViewById(R.id.fab_add_alumno);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecycler.addItemDecoration(new DividerItemDecoration(mRecycler.getContext(), RecyclerView.VERTICAL));
        mRecycler.setAdapter(mAdapter);
        mRecycler.addOnItemTouchListener(new CustomRecyclerListener(getContext(), mRecycler, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, Profile_Fragment.getInstance(mAdapter.getAt(position))).addToBackStack("Lista").commit();
            }

            @Override
            public void onLongClick(View view, int position) {
                showPopup(view, position);
            }
        }));

        mFabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.contentContainer, AddAlumno_Fragment.getInstance(null)).addToBackStack("Lista").commit();
            }
        });

        this.downloadSites();

    }

    private void showPopup(View v, final int position) {
        PopupMenu popup = new PopupMenu(getContext(), v);
        popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.modify_site:
                        modify(mAdapter.getAt(position));
                        //positionClicked = position;
                        return true;
                    case R.id.delete_site:
                        confirm(mAdapter.getAt(position).getEmail(), position);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }

    private void modify(Alumno a) {
        Bundle potatoe = new Bundle();
        potatoe.putSerializable("alumno", a);
        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("Lista").replace(R.id.contentContainer, AddAlumno_Fragment.getInstance(potatoe)).commit();
    }

    private void downloadSites() {
        final ProgressDialog progreso = new ProgressDialog(getContext());
        RestClient.get(URL_API, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                //progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                // Handle resulting parsed JSON response here
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        //put the sites in the adapter
                        mAdapter.set(result.getAlumnos());
                        message = "Connection OK";
                    } else
                        message = result.getMessage();
                else
                    message = "Null data";
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Toast.makeText(getActivity(), throwable.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                Toast.makeText(getActivity(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void confirm(final String idAlumno, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("¿Esta seguro?")
                .setTitle("Borrar Alumno")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        connection(idAlumno, position);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        builder.show();
    }

    private void connection(String id, final int position) {
        final ProgressDialog progreso = new ProgressDialog(getContext());
        RestClient.delete(URL_API + "/" + id, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);
                if (result != null)
                    if (result.getCode()) {
                        message = "Alumno Borrado Correctamente";
                        mAdapter.removeAt(position);
                    } else
                        message = "Error deleting the site:\nEstado: " + result.getStatus() + "\n" +
                                result.getMessage();
                else
                    message = "Null data";

                Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}

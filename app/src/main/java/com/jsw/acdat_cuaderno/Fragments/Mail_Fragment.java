package com.jsw.acdat_cuaderno.Fragments;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.jsw.acdat_cuaderno.Adapter.AlumnoAdapter;
import com.jsw.acdat_cuaderno.Modelo.Email;
import com.jsw.acdat_cuaderno.R;
import com.jsw.acdat_cuaderno.Utils.RestClient;
import com.jsw.acdat_cuaderno.Utils.Result;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class Mail_Fragment extends Fragment {

    public static final String URL_API = "mail";
    public static final int OK = 1;
    private static Mail_Fragment mFragment;
    @BindView(R.id.from)
    EditText from;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.spn_to)
    Spinner to;
    @BindView(R.id.subject)
    EditText subject;
    @BindView(R.id.message)
    EditText message;
    @BindView(R.id.fab_send_mail)
    FloatingActionButton mFabSend;
    private AlumnoAdapter mAdapter;

    public Mail_Fragment() {
        // Required empty public constructor
    }

    public static Mail_Fragment getInstance() {
        if (mFragment == null)
            mFragment = new Mail_Fragment();
        return mFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAdapter = AlumnoAdapter.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_mail, container, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        to.setAdapter(new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, mAdapter.getMails()));
        mFabSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String f = from.getText().toString();
                String p = password.getText().toString();
                String t = to.getSelectedItem().toString();
                String s = subject.getText().toString();
                String m = message.getText().toString();

                if (f.isEmpty() || p.isEmpty() || t.isEmpty()) {
                    Toast.makeText(getContext(), "Fields can not be empty", Toast.LENGTH_SHORT).show();
                } else {
                    Email email = new Email(f, p, t, s, m);
                    connection(email);
                }
            }
        });
    }

    private void connection(Email e) {
        final ProgressDialog progreso = new ProgressDialog(getContext());
        RequestParams params = new RequestParams();
        params.put("from", e.getFrom());
        params.put("password", e.getPassword());
        params.put("to", e.getTo());
        params.put("subject", e.getSubject());
        params.put("message", e.getMessage());

        RestClient.post(URL_API, params, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Connecting...");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) { // Handle resulting parsed JSON response here
                progreso.dismiss();
                Result result;
                Gson gson = new Gson();
                String message;
                result = gson.fromJson(String.valueOf(response), Result.class);

                if (result != null)
                    if (result.getCode()) {
                        Toast.makeText(getContext(), "Mail sent succesfully", Toast.LENGTH_SHORT).show();
                    } else {
                        message = "Error sending the email:\n" + result.getMessage();
                        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                    }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progreso.dismiss();
                Toast.makeText(getContext(), throwable.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

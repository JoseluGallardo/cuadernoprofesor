package com.jsw.acdat_cuaderno.Utils;

import com.jsw.acdat_cuaderno.Modelo.Alumno;
import com.jsw.acdat_cuaderno.Modelo.Falta;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by joselu on 20/02/17.
 */

public class Result implements Serializable {
    boolean code;
    int status;
    String message;
    ArrayList<Alumno> alumnos;
    int last;
    ArrayList<Falta> faltas;

    public boolean getCode() {
        return code;
    }

    public void setCode(boolean code) {
        this.code = code;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(ArrayList<Alumno> students) {
        this.alumnos = alumnos;
    }

    public int getLast() {
        return last;
    }

    public void setLast(int last) {
        this.last = last;
    }

    public ArrayList<Falta> getFaltas() {
        return faltas;
    }

    public void setFaltas(ArrayList<Falta> faltas) {
        this.faltas = faltas;
    }
}

    /*function getCode() {return $this->code;}
    function setCode($c) {$this->code = $c;}
    function getStatus() {return $this->status;}
    function setStatus($s) {$this->status = $s;}
    function getMessage() {return $this->message;}
    function setMessage($m) {$this->message = $m;}
    function getAlumnos() {return $this->alumnos;}
    function setAlumnos($s) {$this->alumnos = $s;}
    function getLast() {return $this->last;}
    function setLast($l) {$this->last = $l;}*/
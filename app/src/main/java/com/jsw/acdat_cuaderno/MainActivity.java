package com.jsw.acdat_cuaderno;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.jsw.acdat_cuaderno.Fragments.Alumno_Fragment;
import com.jsw.acdat_cuaderno.Fragments.Faltas_Fragment;
import com.jsw.acdat_cuaderno.Fragments.Mail_Fragment;
import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Intent i;
    private BottomBar mBottomBar;
    private List<Fragment> mFragments;
    private FragNavController mFragNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFragments = new ArrayList<>(4);
        mFragments.add(Alumno_Fragment.getInstance());
        mFragments.add(Faltas_Fragment.getInstance());
        mFragments.add(Mail_Fragment.getInstance());
        mBottomBar = (BottomBar) findViewById(R.id.bottomBar);
        mBottomBar.setPadding(0, 0, 0, 5);
        mFragNav = new FragNavController(savedInstanceState, getSupportFragmentManager(), R.id.contentContainer, mFragments, FragNavController.TAB1);
        mBottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (getSupportFragmentManager().getBackStackEntryCount() >= 1)
                    getSupportFragmentManager().popBackStack();

                switch (tabId) {
                    case R.id.tab_alumno:
                        mFragNav.switchTab(FragNavController.TAB1);
                        getSupportActionBar().setTitle("Alumnos");
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorAlumnos)));
                        break;
                    case R.id.tab_faltas:
                        mFragNav.switchTab(FragNavController.TAB2);
                        getSupportActionBar().setTitle("Faltas");
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorFaltas)));
                        break;
                    case R.id.tab_email:
                        mFragNav.switchTab(FragNavController.TAB3);
                        getSupportActionBar().setTitle("Email");
                        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorEmail)));
                        break;
                }
            }
        });
    }
}

package com.jsw.acdat_cuaderno.Modelo;

import java.io.Serializable;

/**
 * Created by joselu on 20/02/17.
 */

public class Alumno implements Serializable {
    String imagen;
    String nombre;
    String apellido;
    String direccion;
    String ciudad;
    String codpostal;
    String telefono;
    String email;

    public Alumno(String imagen, String nombre, String apellido, String direccion, String ciudad, String codPostal, String telefono, String email) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.apellido = apellido;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.codpostal = codPostal;
        this.telefono = telefono;
        this.email = email;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCodPostal() {
        return codpostal;
    }

    public void setCodPostal(String codPostal) {
        this.codpostal = codPostal;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

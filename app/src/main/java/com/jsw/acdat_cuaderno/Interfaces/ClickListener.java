package com.jsw.acdat_cuaderno.Interfaces;

import android.view.View;

/**
 * Created by joselu on 20/02/17.
 */

public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}

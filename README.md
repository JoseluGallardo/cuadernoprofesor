# README #

Documentación de la tarea final de Acceso a Datos: Cuaderno del profesor.
José Luis del Pino Gallardo

## Pestaña Alumnos ##

### Visualiza la lista de alumnos en un RecyclerView con el nombre y apellido y una foto del alumno del alumno:

![Screenshot_1488463905.png](https://bitbucket.org/repo/My8Xj4/images/402167773-Screenshot_1488463905.png)

### Una pulsación sobre un alumno nos permite ver su información detallada:

![Screenshot_1488463911.png](https://bitbucket.org/repo/My8Xj4/images/2244413771-Screenshot_1488463911.png)

### Una pulsación larga sobre un alumno nos permite modificar o eliminar el alumno:

![Screenshot_1488463918.png](https://bitbucket.org/repo/My8Xj4/images/1226724403-Screenshot_1488463918.png)

### La opción de modificar un alumno abre el menú en modo editar:

![Screenshot_1488463928.png](https://bitbucket.org/repo/My8Xj4/images/4002978020-Screenshot_1488463928.png)

### El botón flotante abre el menú en modo añadir:
![Screenshot_1488463933.png](https://bitbucket.org/repo/My8Xj4/images/3431182746-Screenshot_1488463933.png)



## Pestaña faltas ##

### Nos muestra el listado de las faltas de los alumnos ese día, en un RecyclerView horizontal. Las faltas se renuevan todos los días a las 12 de la noche:

![Screenshot_1488463939.png](https://bitbucket.org/repo/My8Xj4/images/4043784259-Screenshot_1488463939.png)

###Pulsar sobre una falta de un alumno permitirá editar su falta. Las opciones de cada apartado son cargadas en un spinner para facilitar tiempo al maestro:

![Screenshot_1488463988.png](https://bitbucket.org/repo/My8Xj4/images/3541840485-Screenshot_1488463988.png)

## Pestaña Email ##

### Permite enviar un email mediante gmail a sus alumnos. Los emails de envio están cargados en un spinner para evitar confusión al maestro.

![Screenshot_1488463996.png](https://bitbucket.org/repo/My8Xj4/images/913516548-Screenshot_1488463996.png)